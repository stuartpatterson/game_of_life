#==============================================================================
#
# conway game of life 
#   per rules at https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
#   written by Stuart Patterson (2022) do with it what you will...
#
# coded using python 3.9.2
# pip install pygame
# pip install numpy
#
#==============================================================================
import pygame
from enum import Enum
import numpy as np
import random

class GameState(Enum):
    MENU = 0
    CELL_PLACEMENT = 1
    F1_PRESSED = 2
    PLAYING = 3
    QUIT = 4

# the playfield will be a 3-dimensional numpy array which will be two copies of the playfield,
# per conways rules, each tick requires that we look at the playfield as a whole.  thus we make
# life/death changes on the inactive playfield, when done we flip the inactive to active and
# we repeat - thus active_playfield xor 1 toggles between 0, to 1, and back to 0 - page flipping the playfield
playfield_x_width = 100
playfield_y_height = 100
playfield = np.zeros((2, playfield_x_width, playfield_y_height), dtype=bool)   
active_playfield = 0 

life_cycles = 0

FOOTER = 50    # this is added to the HEIGHT
WIDTH = 600     # screen width
HEIGHT = 600+FOOTER    # and height
screen_size = (WIDTH, HEIGHT)
BLACK = (0X0,0X0,0x0)
WHITE = (0xff, 0xff, 0xff)

scale_factor_width = WIDTH / playfield_x_width
scale_factor_height = (HEIGHT-FOOTER) / playfield_y_height
spirit_center_adj = scale_factor_width // 2   #this should be the center of the spirit on the screen

# we start off with placing cells before we starting the game of life
current_game_state = GameState.MENU
last_game_state = current_game_state
cursor_x_position = int(playfield_x_width/2) # place cursor in the center of the playfield
cursor_y_position = int(playfield_y_height/2)

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def randomInitPlayField():
    global life_cycles
    
    life_cycles = 0

    for x in range(playfield_x_width):
        for y in range(playfield_y_height):
            v = bool(random.randint(0,1))
            playfield[active_playfield,x,y] = v

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def clearPlayField():
    global life_cycles
    
    life_cycles = 0

    for x in range(playfield_x_width):
        for y in range(playfield_y_height):
            playfield[active_playfield,x,y] = False            

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def getCellNeighbourCount( pos ):
    neighbour_count = 0
    x = pos[0]
    y = pos[1]

    # we need to look at all eight neighbours around this cell 
    # to see if we have any live neighbours

    # per calc these values so we don't have repeated overhead
    # for each function call
    norm_x = normalizeX(x)
    norm_x_plus_one = normalizeX(x+1)
    norm_x_minus_one = normalizeX(x-1)

    norm_y = normalizeY(y)
    norm_y_plus_one = normalizeY(y+1)
    norm_y_minus_one = normalizeY(y-1)

    # look left
    if playfield[active_playfield, norm_x_minus_one, norm_y] == True:
        neighbour_count +=1

    # look right
    if playfield[active_playfield, norm_x_plus_one, norm_y] == True:
        neighbour_count +=1

    # look up
    if playfield[active_playfield, norm_x, norm_y_minus_one] == True:
        neighbour_count +=1

    #look down
    if playfield[active_playfield, norm_x, norm_y_plus_one] == True:
        neighbour_count +=1

    # if the cell has 4 or more neighbours the cell will die
    # we don't need to know if there are more than 4 neighbours since it 
    # does not change the behavior.  For speed we can use short-cutting in the 
    # conditional statement

    #look left-up
    if neighbour_count < 4 and playfield[active_playfield, norm_x_minus_one, norm_y_minus_one] == True:
        neighbour_count +=1

    #look left-down
    if neighbour_count < 4 and playfield[active_playfield, norm_x_minus_one, norm_y_plus_one] == True:
        neighbour_count +=1

    #look right-up
    if neighbour_count < 4 and playfield[active_playfield, norm_x_plus_one, norm_y_minus_one] == True:
        neighbour_count +=1

    #look right-down
    if neighbour_count < 4 and playfield[active_playfield, norm_x_plus_one, norm_y_plus_one] == True:
        neighbour_count +=1

    return(neighbour_count)

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def normalizeX(x):

    # the order of these conditionals is for speed.  most cells are NOT at the edges!
    if x >= 0 and x < playfield_x_width:
        return(x)
    elif x >= playfield_x_width:
        return(0)
    elif x < 0:
        return(playfield_x_width-1)

    return

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def normalizeY(y):
    
    # the order of these conditionals is for speed.  most cells are NOT at the edges!
    if y >=0 and y < playfield_y_height:
        return(y)
    elif y >= playfield_y_height:
        return(0)
    elif y < 0:
        return(playfield_y_height-1)

    return

#------------------------------------------------------------------------------
# pos is a tuple in the form of (x,y)
#------------------------------------------------------------------------------
def normalizeXY( pos ):
    ret_x = normalizeX(pos[0])
    ret_y = normalizeY(pos[1])
    
    return( (ret_x, ret_y) )

#------------------------------------------------------------------------------
# pos is a tuple in the form of (x,y)
#------------------------------------------------------------------------------
def playfield2screen(game_pos):
    screen_x = (game_pos[0]*scale_factor_width)+spirit_center_adj
    screen_y = (game_pos[1]*scale_factor_height)+spirit_center_adj

    return( (screen_x, screen_y) )

#------------------------------------------------------------------------------
# pos is a tuple in the form of (x,y)
#------------------------------------------------------------------------------
def screen2playfield(screen_pos):
    playfield_x = int(screen_pos[0] /  scale_factor_width)
    playfield_y = int(screen_pos[1] / scale_factor_height)

    return( (playfield_x, playfield_y) )

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def change_playfield_cell(pos, cell_state):
    playfield_pos = screen2playfield(pos)
    playfield[active_playfield, playfield_pos[0], playfield_pos[1]] = cell_state

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def showFrameRate():
    fps=int(clock.get_fps())
    img = font_basic_text.render("FPS: "+str(fps), True, WHITE)
    screen.blit(img, (500, (HEIGHT-FOOTER)+10))

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def draw():
    global screen, font

    screen.fill(BLACK)

    if current_game_state == GameState.MENU:
        img = font_title_text.render("Conway's Game of Life", True, WHITE)
        screen.blit(img, (100, 100))
        img = font_basic_text.render("Press Spacebar to Start", True, WHITE)
        screen.blit(img, (200, 200))

    elif current_game_state == GameState.CELL_PLACEMENT:
        img = font_basic_text.render("Use mouse to draw (left button) and erase (right button) cells", True, WHITE)
        screen.blit(img, (60, (HEIGHT-FOOTER)))
        img = font_basic_text.render("F1: Start/Stop Life Cycle  F2: Generate Random Cells  F3: Clear Board", True, WHITE)
        screen.blit(img,(30, (HEIGHT-FOOTER)+20))
        for x in range(playfield_x_width):
            for y in range(playfield_y_height):
                if playfield[active_playfield, x, y] == True:
                    xy_screen = playfield2screen( (x, y) )
                    pygame.draw.circle(screen, WHITE, (xy_screen[0], xy_screen[1]), spirit_center_adj)


    elif current_game_state == GameState.PLAYING:
        img = font_basic_text.render("Life Cycle: "+str(life_cycles), True, WHITE)
        screen.blit(img, (10, (HEIGHT-FOOTER)+10))
        for x in range(playfield_x_width):
            for y in range(playfield_y_height):
                if playfield[active_playfield, x, y] == True:
                    xy_screen = playfield2screen( (x, y) )
                    pygame.draw.circle(screen, WHITE, (xy_screen[0], xy_screen[1]), spirit_center_adj)

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
def update():
    global cursor_x_position, cursor_y_position
    global current_game_state, last_game_state
    global playfield, active_playfield
    global life_cycles

    keys = pygame.key.get_pressed()
    if keys[pygame.K_ESCAPE]:
        current_game_state = GameState.QUIT

    if current_game_state == GameState.PLAYING:
        if keys[pygame.K_F1]:
            last_game_state = current_game_state
            current_game_state = GameState.F1_PRESSED

        #using the two playfields we look at the active one and build the non-active one
        next_playfield = active_playfield ^ 1 # XOR toggle between 0 and 1
        
        for x in range(playfield_x_width):
            for y in range(playfield_y_height):
                
                if playfield[active_playfield, x, y]:   # live cell
                    neighbours = getCellNeighbourCount( (x,y) )
                    if neighbours < 2 or neighbours > 3:    # we must die from over crowding
                        playfield[next_playfield, x, y] = False
                    else:
                        playfield[next_playfield, x, y] = True     
                else:   # dead cell
                    neighbours = getCellNeighbourCount( (x,y) )
                    if neighbours == 3:    # reproduction!
                        playfield[next_playfield, x, y] = True                    
                    else:
                        playfield[next_playfield, x, y] = False

        active_playfield = next_playfield
        life_cycles += 1

    elif current_game_state == GameState.MENU:
        if keys[pygame.K_SPACE]:
            current_game_state = GameState.CELL_PLACEMENT
        
    elif current_game_state == GameState.CELL_PLACEMENT:
        # have any keys been pressed?
        if keys[pygame.K_F1]:
            last_game_state = current_game_state
            current_game_state = GameState.F1_PRESSED
        elif keys[pygame.K_F2]:
            randomInitPlayField()
        elif keys[pygame.K_F3]:
            clearPlayField()

        #what about mouse buttons to draw and remove cells
        mbutton_left, mbutton_middle, mbutton_right = pygame.mouse.get_pressed()
        if mbutton_left:
            change_playfield_cell(pygame.mouse.get_pos(), True)
        elif mbutton_right:
            change_playfield_cell(pygame.mouse.get_pos(), False)


    elif current_game_state == GameState.F1_PRESSED: # allows for user to release f1 - start/stop key
        if not keys[pygame.K_F1]:
            if last_game_state == GameState.PLAYING:
                current_game_state = GameState.CELL_PLACEMENT
            elif last_game_state == GameState.CELL_PLACEMENT:
                current_game_state = GameState.PLAYING

#******************************************************************************                   
# start of application
#******************************************************************************                   
pygame.init()
pygame.font.init()
font_basic_text = pygame.font.SysFont(None, 24)
font_title_text = pygame.font.SysFont(None, 48)
screen = pygame.display.set_mode(screen_size)

pygame.display.set_caption("Conway's Game of Life")
clock = pygame.time.Clock()

while current_game_state != GameState.QUIT:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            current_game_state = GameState.QUIT


    update()
    draw()
    #showFrameRate()
    pygame.display.flip()
    clock.tick(60)

pygame.font.quit()
pygame.quit()